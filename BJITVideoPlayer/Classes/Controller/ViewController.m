//
//  ViewController.m
//  BJITVideoPlayer
//
//  Created by Masum on 10/5/18.
//  Copyright © 2018 Masum. All rights reserved.
//

#import "ViewController.h"
#import "MediaPlayerCollectionView.h"
#import "VideoItem.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet MediaPlayerCollectionView *videoPlayerCollectionView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.videoPlayerCollectionView addDataToCollectionView:[self createDataSourceForMediaList]];
}

- (NSArray *)createDataSourceForMediaList
{
    VideoItem *item = [[VideoItem alloc] initWithUrl:@"1" withName:@"Fist Item"];
    NSMutableArray *itemList = [NSMutableArray arrayWithCapacity:0];
    [itemList addObject:item];
    item = [[VideoItem alloc] initWithUrl:@"2" withName:@"Second Item"];
    [itemList addObject:item];
    item = [[VideoItem alloc] initWithUrl:@"3" withName:@"Third Item"];
    [itemList addObject:item];
    item = [[VideoItem alloc] initWithUrl:@"4" withName:@"Forth Item"];
    [itemList addObject:item];
    return itemList;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
