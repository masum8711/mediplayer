//
//  VideoPlayerCollectionViewCell.m
//  BJITVideoPlayer
//
//  Created by Masum on 10/5/18.
//  Copyright © 2018 Masum. All rights reserved.
//

#import "VideoPlayerCollectionViewCell.h"
#import "MediaPlayerViewNode.h"

@interface VideoPlayerCollectionViewCell()
@property (weak, nonatomic) IBOutlet MediaPlayerViewNode *mediaNode;

@end
@implementation VideoPlayerCollectionViewCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setUpInformationWithVideoModel:(VideoItem *)video
{
    [self.mediaNode setInformation:video];
}

@end
