//
//  MediaPlayerCollectionView.m
//  BJITVideoPlayer
//
//  Created by Masum on 10/5/18.
//  Copyright © 2018 Masum. All rights reserved.
//

#import "MediaPlayerCollectionView.h"
#import "VideoPlayerCollectionViewCell.h"

@interface MediaPlayerCollectionView ()<UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIScrollViewDelegate>

@property (strong, nonatomic) UICollectionViewFlowLayout *flowLayout;
@property (strong, nonatomic) NSMutableArray *mediaList;

@end
@implementation MediaPlayerCollectionView

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self setupView];
    }
    
    return self;
}
- (void)addDataToCollectionView:(NSArray *)mediaList
{
    [self.mediaList addObjectsFromArray:mediaList];
    [self reloadData];
}

- (void)setupView
{
    self.flowLayout = [[UICollectionViewFlowLayout alloc] init];
    self.flowLayout.itemSize = CGSizeMake(self.frame.size.width, 220);
    [self.flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [self setCollectionViewLayout:self.flowLayout];
    
    self.delegate = self;
    self.dataSource = self;
    self.mediaList = [[NSMutableArray alloc] initWithCapacity:0];
    [self registerXibForDisplay];
}

- (void)registerXibForDisplay {
    [self registerNib:[VideoPlayerCollectionViewCell nib] forCellWithReuseIdentifier:NSStringFromClass([VideoPlayerCollectionViewCell class])];
    
}
#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.mediaList count] * 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.item;
    if(index > self.mediaList.count - 1){
        index -= self.mediaList.count;
    }
    
    VideoPlayerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([VideoPlayerCollectionViewCell class]) forIndexPath:indexPath];
    VideoItem *item = [self.mediaList objectAtIndex:(index % self.mediaList.count)];
    [cell setUpInformationWithVideoModel:item];
    return cell;
}


#pragma mark - UIScrollView Delegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    int scrollViewWidth = scrollView.contentSize.width;
    
    CGPoint offset = scrollView.contentOffset;
    if(offset.x < scrollViewWidth /4){
        offset.x += scrollViewWidth / 2;
        [scrollView setContentOffset:offset];
    } else if(offset.x > scrollViewWidth /4 *3){
        offset.x -= scrollViewWidth / 2;
        [scrollView setContentOffset:offset];
    }
}
@end
