//
//  MediaPlayerViewNode.h
//  BJITVideoPlayer
//
//  Created by Masum on 10/5/18.
//  Copyright © 2018 Masum. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoItem.h"

@interface MediaPlayerViewNode : UIView

- (void)setInformation:(VideoItem *)item;
- (void)playVideo;

@end
