//
//  MediaPlayerViewNode.m
//  BJITVideoPlayer
//
//  Created by Masum on 10/5/18.
//  Copyright © 2018 Masum. All rights reserved.
//

#import "MediaPlayerViewNode.h"
#import <AVFoundation/AVFoundation.h>

@interface MediaPlayerViewNode() {
    
    NSObject * periodicPlayerTimeObserverHandle;
}

@property (weak, nonatomic) IBOutlet UIView *playerNode;
@property (weak, nonatomic) IBOutlet UISlider *progressSlider;
@property (weak, nonatomic) IBOutlet UIButton *btnPlay;

@property (strong, nonatomic) AVPlayer *player;

@end

@implementation MediaPlayerViewNode

#pragma mark - Xib Initializer

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self addXibView];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self addXibView];
    }
    return self;
}

- (void)addXibView {
    
    NSArray *xibs = [[NSBundle mainBundle] loadNibNamed: NSStringFromClass([MediaPlayerViewNode class]) owner:self options:nil];
    UIView *_view = xibs.firstObject;
    _view.frame = self.bounds;
    [self addSubview:_view];
}

#pragma mark - Information Management

- (void)setInformation:(VideoItem *)item{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:[item getVideoDirectory]  ofType:@"mp4"];
    AVPlayerItem *avPlayerItem = [[AVPlayerItem alloc] initWithURL:[NSURL fileURLWithPath:path]];
    self.player = [[AVPlayer alloc] initWithPlayerItem:avPlayerItem];

    AVPlayerLayer *playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
    playerLayer.frame = self.frame;
    playerLayer.name = @"videoLayer";
    
    for (CALayer *layer in self.playerNode.layer.sublayers) {
        if([layer.name isEqualToString:@"videoLayer"]){
            [layer removeFromSuperlayer];
        }
    }
    [self.playerNode.layer addSublayer:playerLayer];
    [self.btnPlay setBackgroundImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    [self.progressSlider setValue:0.0f];
}

- (IBAction)audioSliderDragged:(id)sender {
    
   float maxValue = CMTimeGetSeconds(_player.currentItem.asset.duration);
   float durationToSeek = maxValue * self.progressSlider.value;
   [self.player seekToTime:CMTimeMakeWithSeconds(durationToSeek, self.player.currentItem.duration.timescale)];
 
}

- (void)playVideo {
    
    [self.btnPlay setBackgroundImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[_player currentItem]];
    
    __weak typeof(self) weakSelf = self;
    periodicPlayerTimeObserverHandle = [self.player addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(1.0 / 60.0, NSEC_PER_SEC)
                                                                             queue:NULL
                                                                        usingBlock:^(CMTime time){
                                                                            [weakSelf updateProgressBar];
                                                                        }];
    [self.player play];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    
    if ([notification.object isEqual:self.player.currentItem])
    {
        [self.player seekToTime:kCMTimeZero];
        [self.player pause];
        _progressSlider.value = 0;
         [self.btnPlay setTitle:@"Play" forState:UIControlStateNormal];
    }
}

- (void)updateProgressBar
{
    double currentTime = CMTimeGetSeconds(_player.currentTime);
    if(currentTime <= 0.05){
        _progressSlider.value =  0;
        return;
    }
    
    if (isfinite(currentTime) && (currentTime > 0))
    {
        float maxValue = CMTimeGetSeconds(_player.currentItem.asset.duration);
        _progressSlider.value =  (1/maxValue) * currentTime;
    }
}

-(void) stopPlaying
{
    @try {
        
        if(periodicPlayerTimeObserverHandle != nil)
        {
            [_player removeTimeObserver:periodicPlayerTimeObserverHandle];
            periodicPlayerTimeObserverHandle = nil;
        }
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
        [_player pause];
    }
    @catch (NSException * __unused exception) {}
}

- (BOOL)isPlaying
{
    return ([self.player rate] > 0);
}

- (IBAction)btnPlay:(id)sender {
    
    if (self.player.rate == 0) {
         [self.player play];
        [self.btnPlay setBackgroundImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
    } else {
         [self.player pause];
        self.player.rate = 0;
        [self.btnPlay setBackgroundImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
    }
}

-(void) dealloc{
    
    @try {
        [_player removeObserver:self forKeyPath:@"status"];
    }
    @catch (NSException * __unused exception) {}
    [self stopPlaying];
    _player = nil;
}

@end
