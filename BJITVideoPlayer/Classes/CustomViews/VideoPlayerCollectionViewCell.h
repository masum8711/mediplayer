//
//  VideoPlayerCollectionViewCell.h
//  BJITVideoPlayer
//
//  Created by Masum on 10/5/18.
//  Copyright © 2018 Masum. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoItem.h"

@interface VideoPlayerCollectionViewCell : UICollectionViewCell
+ (UINib *)nib;
- (void)setUpInformationWithVideoModel:(VideoItem *)video;
@end
