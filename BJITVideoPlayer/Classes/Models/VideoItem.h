//
//  VideoItem.h
//  BJITVideoPlayer
//
//  Created by Masum on 10/5/18.
//  Copyright © 2018 Masum. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, MediaPlayerState){
    MediaPlayerStateInitial = 0,
    MediaPlayerStatePlaying = 1,
    MediaPlayerStatePause = 2,
    MediaPlayerStateStop = 3,
};

@interface VideoItem : NSObject<NSCopying,NSCoding>
- (instancetype)initWithUrl:(NSString *)url withName:(NSString *)videoName ;
- (NSString *)getVideoDirectory;
- (NSString *)getVideoName;
- (MediaPlayerState) getVideoPlayingState;
- (double)getCurrentPlayingTime;
- (void)setCurrentTime:(double)currentTime;
- (void)setMediaPlayerState:(MediaPlayerState)mediaPlayerState;
@end
