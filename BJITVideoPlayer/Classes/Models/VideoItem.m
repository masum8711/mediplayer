//
//  VideoItem.m
//  BJITVideoPlayer
//
//  Created by Masum on 10/5/18.
//  Copyright © 2018 Masum. All rights reserved.
//

#import "VideoItem.h"

NSString * const kVMKeyURL = @"url";
NSString * const kVMKeyName = @"name";
NSString * const kVMKeyTime = @"url";
NSString * const kVMKeyState = @"state";

@interface VideoItem()

@property(nonatomic , strong) NSString *videoName;
@property(nonatomic , strong) NSString *videoDirectory;
@property(nonatomic , assign) MediaPlayerState mediaPlayerState;
@property(nonatomic , assign) double currentTime;

@end

@implementation VideoItem

- (instancetype)initWithUrl:(NSString *)url withName:(NSString *)videoName {
    
    self = [super init];
    
    if (self) {
        _videoDirectory = [url copy];
        _videoName = [videoName copy];
        _mediaPlayerState = MediaPlayerStateInitial;
        _currentTime = 0.0;
    }
    
    return self;
}

- (NSString *)getVideoDirectory
{
    return self.videoDirectory;
}

- (NSString *)getVideoName
{
    return self.videoName;
}

- (MediaPlayerState) getVideoPlayingState
{
    return self.mediaPlayerState;
}

- (double)getCurrentPlayingTime
{
    return self.currentTime;
}

- (void)setCurrentTime:(double)currentTime
{
    _currentTime = currentTime;
}

- (void)setMediaPlayerState:(MediaPlayerState)mediaPlayerState
{
    _mediaPlayerState = mediaPlayerState;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init]) {
        
        self.videoDirectory = [aDecoder decodeObjectOfClass:[NSString class] forKey:kVMKeyURL];
        self.videoName = [aDecoder decodeObjectOfClass:[NSString class] forKey:kVMKeyName];
        self.mediaPlayerState = [aDecoder decodeIntegerForKey:kVMKeyState];
        self.currentTime = [aDecoder decodeIntegerForKey:kVMKeyTime];

    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.videoDirectory forKey:kVMKeyURL];
    [aCoder encodeObject:self.videoName forKey:kVMKeyName];
    [aCoder encodeInteger:self.mediaPlayerState forKey:kVMKeyState];
    [aCoder encodeDouble:self.currentTime forKey:kVMKeyTime];
}

- (instancetype)copyWithZone:(NSZone *)zone
{
    return [NSKeyedUnarchiver unarchiveObjectWithData:[NSKeyedArchiver archivedDataWithRootObject:self]];
}

@end
